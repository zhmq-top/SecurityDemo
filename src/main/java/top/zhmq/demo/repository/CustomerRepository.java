package top.zhmq.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.zhmq.demo.domain.Customer;

/**
 * TODO 在此处添加描述
 *
 * @author Glory
 * @classname CustomerRepository
 * @create 2021-04-26 11:00
 */

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    Customer findByUsername(String username);
}