package top.zhmq.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.zhmq.demo.domain.Authority;

import java.util.List;

/**
 * TODO 在此处添加描述
 *
 * @author Glory
 * @classname AuthorityRepository
 * @create 2021-04-26 11:15
 */

public interface AuthorityRepository extends JpaRepository<Authority,Integer> {
    @Query(value = "select a.* from t_customer c,t_authority a,t_customer_authority ca where ca.customer_id=c.id and ca.authority_id=a.id and c.username =?1",nativeQuery = true)
    public List<Authority> findAuthoritiesByUsername(String username);
}
