package top.zhmq.demo.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 测试注解实现访问控制类
 *
 * @author Glory
 * @classname TestController
 * @create 2021-04-26 10:42
 */

@Controller
public class TestController {
    //@Secured注解配置访问当前方法的角色信息
    @GetMapping("/role1/vip")
    @ResponseBody
    @Secured({"ROLE_vip"})
    /*
    经测试，@Secured中角色需要加上ROLE_，否则出错
    即，ROLE_vip可以，而vip不可以
     */
    public String auth1_admin() {
        return "此界面为拥有ROLE_vip才能访问到的界面，采用@Secured注解配置访问控制";
    }


    @GetMapping("/role1/common")
    @ResponseBody
    @Secured({"ROLE_vip", "ROLE_common"})
    public String auth1_user() {
        return "此界面为拥有ROLE_vip或ROLE_common能访问到的界面，采用@Secured注解配置访问控制";
    }

    @GetMapping("/role2/vip")
    @ResponseBody
    @PreAuthorize("hasAuthority('ROLE_vip')")  //方法进入前的权限验证
    /*
    经测试，@PreAuthorize中角色需要加上ROLE_，否则出错
    即，ROLE_vip可以，而vip不可以
     */
    public String auth2_admin() {
        return "此界面为拥有ROLE_vip才能访问到的界面，采用@PreAuthorize注解配置访问控制，是方法进入前的权限验证";
    }

    @GetMapping("/role2/common")
    @ResponseBody
    @PostAuthorize("hasAnyAuthority('ROLE_vip','ROLE_common')") //方法执行后进行权限验证，使用较少，一般用于对返回值的验证
    /*
    经测试，@PostAuthorize中角色需要加上ROLE_，否则出错
    即，ROLE_vip可以，而vip不可以
     */
    public String auth2_user() {
        return "此界面为拥有ROLE_vip或ROLE_common能访问到的界面，采用@PostAuthorize注解配置访问控制，是方法执行后进行权限验证，使用较少，一般用于对返回值的验证";
    }

    @GetMapping("/onlyself")
    @ResponseBody
    @PreAuthorize("principal.username.equals(#username)")
    /*
    限制只能查询自己的信息
     */
    public String self(String username) {
        return "此界面，只能查询自己的信息，您的信息为"+username;
    }

    @GetMapping("/errorpage")
    @ResponseBody
    public String five() {
        int a = 5 / 0;
        return "something";
    }

    @GetMapping("/test")
    @ResponseBody
    public String ip(){
        return "测试403界面";
    }
//    @PostFilter权限验证之后对数据进行过滤
//    @PostFilter("filterObject.username == 'jack'")
//    @PreFilter:进入控制器之前对数据进行过滤
}
