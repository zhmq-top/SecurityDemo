package top.zhmq.demo.config;

import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Objects;

/**
 * TODO 在此处添加描述
 *
 * @author Glory
 * @classname MyPasswordEncoder
 * @create 2021-04-28 19:11
 */

public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        String  s1 = charSequence.toString();
        return Objects.equals(s1, s);
    }
}
