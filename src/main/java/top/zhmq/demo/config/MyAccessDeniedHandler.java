package top.zhmq.demo.config;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 自定义403处理方法
 *
 * @author Glory
 * @classname MyAccessDeniedHandler
 * @create 2021-04-26 10:58
 */

@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {
    //自定义拒绝访问异常处理
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.sendRedirect("/error/403");
    }
}