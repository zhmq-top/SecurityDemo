# SpringSecurity的Demo项目
## 项目预览
已发布至服务器。[点击直接进入](http://42.193.99.89:8088 "点击直接进入")

测试用的账号密码首页会显示

## 运行说明
1. 配置Redis，(redis我未开启公网访问，所以配一下吧)
2. 配置数据库：运行static文件夹下的SecurityDemo.sql，并修改账号密码(也可以不配，因为直接连上我的服务器了，请勿直接修改数据库！！！)
3. 启动项目，访问http://localhost:8088 (或者指定端口)

## 访问说明

<img class="aligncenter size-full wp-image-1171" src="https://zhmq.top/wp-content/uploads/2021/04/2021042700362049.png" alt="" />

vip用户：

<img class="aligncenter size-full wp-image-1172" src="https://zhmq.top/wp-content/uploads/2021/04/202104270036365.png" alt="" />

## 前端技术
1. CSS
2. HTML
3. JavaScript
4. JQuery
5. BootStrap

## 后端技术
1. MySQL
2. JPA
3. SpringBoot
4. SpringSecurity
5. Redis
6. Thymeleaf
7. Lombook
8. Maven
9. JDBC

## 内容简介
本项目为SpringSecurity的演示项目，实现的演示功能有：
1. 安全管理
2. 自定义用户认证：
	- 内存身份认证
	- JDBC身份认证
	- UserDetailsService身份认证
3. 自定义用户授权管理
		
		a) 自定义登陆成功处理类
		b) 自定义登陆失败处理类
		c) 自定义拒绝访问界面
		d) 自定义登录界面
		e) 自定义用户退出
		
4. 登录信息获取
5. 记住我功能
6. CSRF防护
7. Security管理前端界面